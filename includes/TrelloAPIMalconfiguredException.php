<?php
/**
 * @file
 * Contains the TrelloAPIMalconfiguredException exception.
 */

/**
 * Exception for when TrelloAPI class is misconfigured.
 */
class TrelloAPIMalconfiguredException extends Exception {}
